# Define the provider. In this case we're building in gitlab
provider "gitlab" {
  base_url = "http://asv-nyccgit01.county.nycc.internal/api/v4/"
}



// create a group
resource "gitlab_group" "covid" {
  name        = "Example group"
  path        = "example-group"
  description = "An example group"
}


// Create a project for each top level workstream
resource "gitlab_project" "example" {
  count        = length(var.projects)
  name         = var.projects[count.index].name
  path         = var.projects[count.index].path
  description  = var.projects[count.index].description
  namespace_id    = gitlab_group.covid.id
}

