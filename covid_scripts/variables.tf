variable "projects" {
    description = "What we're calling this network thing we're making"
    type = list(object({
        name = string
        path = string
        description = string
    }))
    default = [{
        name = "Test Project"
        path = "test-project"
        description = "test project description"
    }]
}



