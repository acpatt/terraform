## Audience
<!--- Please provide specific e.g. internal – CYPS Staff or external – Volunteers --->

## Key Message
<!--- Please include a bullet pointed description of the message --->

* Message 1
* Message 2..

## Channel (Tick all that apply)
<!--- Through which channels does this need to be broadcast?  --->

1. [ ] Intranet
2. [ ] Bulletin (Tuesdays and Fridays)
3. [ ] Email
4. [ ] External Website (Include links to pages required below)
5. [ ] Press (Include specifics below)
6. [ ] Social Media
7. [ ] Key Messages
8. [ ] Video Required (detail below)
9. [ ] Radio
10. [ ] Other

#### Additional Channel details
<!--- Is there anything else that you need to include against your channel choices? --->

## Signposting
<!--- Is there any information required regarding signposting --->


## Signoff
<!--- Who is required to sign off this request? --->

## Workstream PM lead
<!--- Who is the PM lead for the workstream from which this request originated? --->

<!--- 
Please add the following labels:
issue-type:issue-comms-plan
workstream:(Whatever workstream this is in)
substream:(whatever substream this work falls under) (If your workstream has substreams)

Please include a due date to indicate when this is required. 
Please try and give as much notice as possible.
 ---> 
