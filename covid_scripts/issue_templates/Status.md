## Daily status summary
<!--- Update this every day with the Daily status summary. In Daily status summary it's important to include: Date, Daily summary --->
* Date:
* Summary including reason for RAG: 

## Weekly highlight report
<!--- Update this every week with the Weekly highlight report. It's important to include: Date, Daily summary --->
* Date:
* Highlight report including reason for RAG: 
* Key activities from last week:
* Key activities next week:

## Project details
<!--- Please include a summary of the project --->

## Critical Success factors
<!--- How will you know when this has been done? What are the key items which need to be in place to deliver the desired outcomes. --->

1. [ ] Success factor
2. [ ] Success factor
3. [ ] …

<!--- 
Please add the following labels:
issue-type:status
rag-status:(red, amber or green)
project-priority:(a, b, c or d)
project-stage:(startup, definition, delivery, closure)
workstream:(Whatever workstream this is in)
substream:(whatever substream this work falls under) (If your workstream has substreams)
 ---> 
