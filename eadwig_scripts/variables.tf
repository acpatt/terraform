variable "estate_name" {
    description = "What we're calling this network thing we're making"
}

variable "slash_sixteen" {
    description = "what is the first /16 that we're putting this estate in? for example, enter '172.29' for 172.29.0.0/16"
}

variable "app1" {
    description = "what's the name of the first application?"
}
