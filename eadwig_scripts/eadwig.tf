# Define the provider. In this case we're building in azure.

provider "azurerm" {
  # Whilst version is optional, we /strongly recommend/ using it to pin the version of the Provider being used
  version = "=1.28.0"
}

resource "azurerm_resource_group" "estate" {
  name     = "${var.estate_name}-rg"
  location = "West Europe"
}


resource "azurerm_network_security_group" "web-in-nsg" {
  name                = "web-in"
  location            = "${azurerm_resource_group.estate.location}"
  resource_group_name = "${azurerm_resource_group.estate.name}"
  security_rule {
    name                       = "http-in"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "https-in"
    priority                   = 101
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_network_security_group" "nothing-in-nsg" {
  name                = "nothing-in"
  location            = "${azurerm_resource_group.estate.location}"
  resource_group_name = "${azurerm_resource_group.estate.name}"
}


resource "azurerm_network_security_group" "orchestration-in-nsg" {
  name                = "orchestration-in"
  location            = "${azurerm_resource_group.estate.location}"
  resource_group_name = "${azurerm_resource_group.estate.name}"
  security_rule {
    name                       = "ssh-in"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}


resource "azurerm_virtual_network" "dmz" {
  name                = "dmz-vnet"
  resource_group_name = "${azurerm_resource_group.estate.name}"
  location            = "${azurerm_resource_group.estate.location}"
  address_space       = ["172.28.255.0/24"]
  subnet {
    name = "public-in"
    address_prefix = "172.28.255.240/28"
  }
  subnet {
    name = "public-out"
    address_prefix = "172.28.255.224/28"
  }
  subnet {
    name = "${var.app1}-prod-web"
    address_prefix = "172.28.255.208/28"
  }
  subnet {
    name = "private-out"
    address_prefix = "172.28.255.16/28"
    security_group = "${azurerm_network_security_group.nothing-in-nsg.id}" 
  }
  subnet {
    name           = "private-in"
    address_prefix = "172.28.255.0/28"
    security_group = "${azurerm_network_security_group.orchestration-in-nsg.id}"
  }
}

resource "azurerm_app_service_plan" "app1-sp" {
  name                = "${var.app1}-sp"
  location            = "${azurerm_resource_group.estate.location}"
  resource_group_name = "${azurerm_resource_group.estate.name}"
  kind                = "Linux"
  reserved            = true
  sku {
    tier = "Basic"
    size = "B1"
  }
}

resource "null_resource" "docker-compose-app1" {
  provisioner "local-exec" {
      command = "wget -O docker-compose.yml https://gitlab.com/crapheap/Projects/gekko/raw/develop/docker-compose.yml"  
	}
    provisioner "local-exec" {
      command = "az login --service-principal --username $ARM_CLIENT_ID --password $ARM_CLIENT_SECRET --tenant $ARM_TENANT_ID"
        }
    provisioner "local-exec" {
      command = "az webapp create --resource-group ${azurerm_resource_group.estate.name} --plan ${azurerm_app_service_plan.app1-sp.name} --name ${var.estate_name}-${var.app1} --multicontainer-config-type compose --multicontainer-config-file docker-compose.yml"
	}
    provisioner "local-exec" {
      when    = "destroy"
      command = "az webapp delete --resource-group ${azurerm_resource_group.estate.name} --keep-empty-plan --name ${var.estate_name}-${var.app1}"
        }
    provisioner "local-exec" {
      when    = "destroy"
      command = "rm docker-compose.yml"
        }
}
