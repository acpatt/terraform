# Define the provider. In this case we're building in azure.

provider "azurerm" {
  # Whilst version is optional, we /strongly recommend/ using it to pin the version of the Provider being used
  version = "=1.28.0"
}

resource "azurerm_resource_group" "estate" {
  name     = "${var.estate_name}-rg"
  location = "UK South"
}


resource "azurerm_network_security_group" "web-in-nsg" {
  name                = "web-in"
  location            = "${azurerm_resource_group.estate.location}"
  resource_group_name = "${azurerm_resource_group.estate.name}"
  security_rule {
    name                       = "http-in"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "https-in"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_network_security_group" "sql-in-nsg" {
  name                = "sql-in"
  location            = "${azurerm_resource_group.estate.location}"
  resource_group_name = "${azurerm_resource_group.estate.name}"
  security_rule {
    name                       = "sql-in"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "3306"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_network_security_group" "nothing-in-nsg" {
  name                = "nothing-in"
  location            = "${azurerm_resource_group.estate.location}"
  resource_group_name = "${azurerm_resource_group.estate.name}"
}


resource "azurerm_network_security_group" "orcehstration-in-nsg" {
  name                = "orcehstration-in"
  location            = "${azurerm_resource_group.estate.location}"
  resource_group_name = "${azurerm_resource_group.estate.name}"
  security_rule {
    name                       = "ssh-in"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}




resource "azurerm_virtual_network" "dmz" {
  name                = "dmz-vnet"
  resource_group_name = "${azurerm_resource_group.estate.name}"
  location            = "${azurerm_resource_group.estate.location}"
  address_space       = ["172.29.248.0/21"]
  subnet {
    name           = "public-in"
    address_prefix = "172.29.255.240/28"
	security_group = "${azurerm_network_security_group.web-in.id}"
  }
  subnet {
    name           = "public-out"
    address_prefix = "172.29.255.224/28"
  }
  subnet {
    name           = "app1-prod-web"
    address_prefix = "172.29.255.208/28"
	security_group = "${azurerm_network_security_group.web-in.id}"
  }
  subnet {
    name           = "app1-prod-data"
    address_prefix = "172.29.255.192/28"
	security_group = "${azurerm_network_security_group.sql-in.id}"
  }
  subnet {
    name           = "app1-stage-web"
    address_prefix = "172.29.255.176/28"
	security_group = "${azurerm_network_security_group.web-in.id}"
  }
  subnet {
    name           = "app1-stage-data"
    address_prefix = "172.29.255.160/28"
	security_group = "${azurerm_network_security_group.sql-in.id}"
  }
  subnet {
    name           = "private-out"
    address_prefix = "172.29.248.16/28"
	security_group = "${azurerm_network_security_group.nothing-in.id}"
  }
  subnet {
    name           = "private-in"
    address_prefix = "172.29.248.0/28"
	security_group = "${azurerm_network_security_group.orchestration-in.id}"
  }
}

resource "azurerm_virtual_network" "hub" {
  name                = "hub-vnet"
  resource_group_name = "${azurerm_resource_group.hub_vnet.name}"
  location            = "${azurerm_resource_group.hub_vnet.location}"
  address_space       = ["172.29.192.0/22"]
  subnet {
    name           = "provsioning"
    address_prefix = " 172.29.196.192/26"
  }
  subnet {
    name           = "build"
    address_prefix = "172.29.195.0/24"
  }
  subnet {
    name           = "management"
    address_prefix = "172.29.192.16/28"
	security_group = "${azurerm_network_security_group.nothing-in.id}"
  }
  subnet {
    name           = "gateway"
    address_prefix = "172.29.192.0/28"
  }
}